package net.jonter.nycschoolschallenge;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.security.ProviderInstaller;
import com.google.gson.GsonBuilder;

import net.jonter.nycschoolschallenge.models.SATScores;
import net.jonter.nycschoolschallenge.models.Schools;

import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.net.ssl.SSLContext;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MainActivity extends AppCompatActivity
{
    Button refreshButton;
    Button viewDataButton;
    TextView mainText;
    List<Schools> schools;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        ActiveAndroid.initialize(this);
        setContentView(R.layout.activity_main);

        refreshButton = findViewById(R.id.refreshButton);
        viewDataButton = findViewById(R.id.viewDataButton);
        mainText = findViewById(R.id.mainText);
        schools = getSchools();
        mainText.setText(schools.size() + " " + getString(R.string.schools_loaded));
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                getSchoolData();
            }
        });
        viewDataButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View arg0)
            {
                //this needs to be loaded twice, because there's some instances where the data could be loaded but not refreshed in the onClickListener!
                schools = getSchools();
                if(schools.size() > 0)
                {
                    Intent i = new Intent(MainActivity.this, SchoolList.class);
                    startActivityForResult(i, 0);
                }
                else
                {
                    Toast.makeText(MainActivity.this, getString(R.string.refresh_data_first), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private List<Schools> getSchools()
    {
        List<Schools> schools = new Select()
                .from(Schools.class)
                .execute();
        return schools;
    }

    // This method is called when the second activity finishes
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        //result cancelled means we came back from the selector's back button. Sometimes that's a failure, sometimes it's not.
        //todo: make this a little bit more granular and accurate! For now it's okay, but not perfect.
        if (resultCode == RESULT_CANCELED)
        {
        //    mainText.setText(R.string.data_failed); //not ALWAYS a failure! If I had more time I could better test for corrupted data / weird data failures and report them properly here.
        }
        //generic problem loading data, but most probably a DB error
        if(resultCode == RESULT_FIRST_USER)
        {
            mainText.setText(R.string.data_error);
        }
    }



    private void getSchoolData()
    {
        final AtomicBoolean schoolsFinished = new AtomicBoolean(false);
        final AtomicBoolean SATsfinished = new AtomicBoolean(false);

        initializeSSLContext(MainActivity.this);

        mainText.setText(getString(R.string.refreshing));
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://data.cityofnewyork.us/resource/")
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()))
                .build();

        ApiInterface apiService = retrofit.create(ApiInterface.class);

        Call<List<Schools>> callSchools = apiService.getTopSchools();
        callSchools.enqueue(new Callback<List<Schools>>() {
            @Override
            public void onResponse(Call<List<Schools>> call, Response<List<Schools>> response)
            {
                ActiveAndroid.beginTransaction();
                new Delete().from(Schools.class).where("").execute(); //clear out the local DB before adding new

                for(Schools school: response.body())
                {
                    school.save();
                }
                ActiveAndroid.setTransactionSuccessful();
                ActiveAndroid.endTransaction();

                schoolsFinished.set(true);
                doneRefreshing(schoolsFinished, SATsfinished);
            }

            @Override
            public void onFailure(Call<List<Schools>> call, Throwable t) {
                // Log error here since request failed
                schoolsFinished.set(false);
                Toast.makeText(MainActivity.this, getString(R.string.data_failed), Toast.LENGTH_LONG).show();
                Log.e("THIS", t.toString());
                mainText.setText(getString(R.string.error_refreshing));
            }
        });
        Call<List<SATScores>> callSATScores = apiService.getSATScores();
        callSATScores.enqueue(new Callback<List<SATScores>>() {
            @Override
            public void onResponse(Call<List<SATScores>> call, Response<List<SATScores>> response)
            {
                ActiveAndroid.beginTransaction();
                new Delete().from(SATScores.class).where("").execute(); //clear out the local DB before adding new

                for(SATScores score: response.body())
                {
                    score.save();
                }
                ActiveAndroid.setTransactionSuccessful();
                ActiveAndroid.endTransaction();
                SATsfinished.set(true);
                doneRefreshing(schoolsFinished, SATsfinished);
            }

            @Override
            public void onFailure(Call<List<SATScores>> call, Throwable t) {
                SATsfinished.set(false);
                Toast.makeText(MainActivity.this, getString(R.string.data_failed), Toast.LENGTH_LONG).show();
                Log.e("THIS", t.toString());
                mainText.setText(getString(R.string.error_refreshing));
            }
        });
    }
    /*
     * Should fix for some older Android devices, as per:
     * https://stackoverflow.com/a/42471738/1042362
     */
    private static void initializeSSLContext(Context mContext){
        try {
            SSLContext.getInstance("TLSv1.2");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            ProviderInstaller.installIfNeeded(mContext.getApplicationContext());
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }
    private synchronized void doneRefreshing(AtomicBoolean schoolsFinished, AtomicBoolean SATsfinished)
    {
        if(schoolsFinished.get() && SATsfinished.get())
        {
            mainText.setText(R.string.data_successful);
        }
        if(schoolsFinished.get() != SATsfinished.get())
        {
            mainText.setText(R.string.refreshing_1);
        }
    }
}
