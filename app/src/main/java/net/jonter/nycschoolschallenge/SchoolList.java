package net.jonter.nycschoolschallenge;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;

import net.jonter.nycschoolschallenge.models.Schools;

import java.util.ArrayList;
import java.util.List;


public class SchoolList extends AppCompatActivity {

    SchoolAdapter adapter;
    List<Schools> schools;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActiveAndroid.initialize(this);
        setContentView(R.layout.activity_school_list);
        //make sure there's no DB problem when we query
        try{
            schools = getAllSchools();
        }
        catch(Exception e)
        {
            Intent intent = new Intent();
            setResult(RESULT_FIRST_USER, intent);
            e.printStackTrace();
            SchoolList.this.finish();
        }
        //make sure we have schools to display
        if(schools.size()>0)
        {
            adapter = new SchoolAdapter(schools);
            recyclerView = findViewById(R.id.school_list_recycler_view);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
            recyclerView.setAdapter(adapter);
        }
        else
        {
            Intent intent = new Intent();
            setResult(RESULT_CANCELED, intent);
            finish();
        }
    }

    public static List<Schools> getAllSchools()
    {
        return new Select()
                .from(Schools.class)
                //we just want all of them, so...
                //.where("Category = ?", category.getId())
                //.orderBy("school_name ASC")
                .execute();
    }

    public class SchoolAdapter extends RecyclerView.Adapter
    {
        //load the model
        private List<Schools> schools = new ArrayList<>();

        public SchoolAdapter(final List<Schools> viewModels)
        {
            if (viewModels != null)
            {
                this.schools.addAll(viewModels);
            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType)
        {
            View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
            return new SimpleViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position)
        {
            ((SimpleViewHolder) holder).bindData(schools.get(position));
        }

       @Override
        public int getItemCount() {
            return schools.size();
        }
        @Override
        public int getItemViewType(final int position) {
            return R.layout.school_row;
        }
        public class SimpleViewHolder extends RecyclerView.ViewHolder
        {
            private TextView simpleTextView;

            public SimpleViewHolder(final View itemView)
            {
                super(itemView);
                simpleTextView = itemView.findViewById(R.id.school_name);
                itemView.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {

                        Object object= schools.get(getLayoutPosition());
                        Schools school = (Schools)object;

                        Intent intent = new Intent(SchoolList.this, SchoolDetail.class);
                        intent.putExtra("school", school);

                        startActivity(intent);
                    }

                });
            }

           public void bindData(final Schools viewModel) {
                simpleTextView.setText(viewModel.school_name);
            }
        }
    }
}
