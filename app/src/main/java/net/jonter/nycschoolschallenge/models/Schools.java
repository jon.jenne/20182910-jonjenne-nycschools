package net.jonter.nycschoolschallenge.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

import java.io.Serializable;

@Table(name="Schools")
public class Schools extends Model implements Serializable
{

    @Expose
    @Column(name="dbn", index=true)
    public String dbn;

    @Expose
    @Column(name="school_name")
    public String school_name;

    @Expose
    @Column(name="phone_number", index=true)
    public String phone_number;

    @Expose
    @Column(name="primary_address_line_1")
    public String primary_address_line_1;

    @Expose
    @Column(name="latitude")
    public String latitude;

    @Expose
    @Column(name="longitude")
    public String longitude;


    public Schools()
    {

    }
}